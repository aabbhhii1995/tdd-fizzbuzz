import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class FizzBuzzTest {

	@Before
	public void setUp() throws Exception {
	}
	
	//R1 : Number divisible by 3 return "fizz"
	@Test
	public void testDivisibleBy3() {
		FizzBuzz b = new FizzBuzz();
		String result = b.buzz(27);
		assertEquals("fizz", result);
	}
	
	//R2 : Number divisible by 5 return "buzz"
		@Test
		public void testDivisibleBy5() {
			FizzBuzz b = new FizzBuzz();
			String result = b.buzz(10);
			assertEquals("buzz", result);
		}
		
		//R3 : Number divisible by 15 return "buzz"
				@Test
				public void testDivisibleBy15() {
					FizzBuzz b = new FizzBuzz();
					String result = b.buzz(15);
					assertEquals("fizzbuzz", result);
				}

		//R4 : Number not divisible by 3,5,15 return number itself
		@Test
		public void testOtherNumber() {
			FizzBuzz b = new FizzBuzz();
			String result = b.buzz(4);
			assertEquals("4", result);
		}
		//R5 : Number is prime return whizz
				@Test
				public void testPrimeNumber() {
					FizzBuzz b = new FizzBuzz();
					String result = b.buzz(11);
					assertEquals("whizz", result);
				}

	//R6: if number is prime and divisible by 3
				//append 'whizz' to end
				//expected output 3: fizzwhizz
				//5: buzzwhizz
			
				@Test
				public void testAppendWhizz() {
					FizzBuzz b = new FizzBuzz();
					//3:
					String result = b.buzz(3);
					assertEquals("fizzwhizz", result);
					//5:
					result = b.buzz(5);
					assertEquals("buzzwhizz", result);
				}
				
		
				
}
